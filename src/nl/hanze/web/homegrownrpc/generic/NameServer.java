package nl.hanze.web.homegrownrpc.generic;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class NameServer
{
	private ServerSocket ssoNameServer;

	@SuppressWarnings("rawtypes")
	private HashMap<String, Class> hsmServerClass;
	private HashMap<String, String> hsmServerIP;
	private HashMap<String, Integer> hsmServerPort;

	/**
	 * 
	 * @param port
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public NameServer(int port) throws Exception
	{
		ssoNameServer = new ServerSocket(port);
		hsmServerClass = new HashMap<String, Class>();
		hsmServerIP = new HashMap<String, String>();
		hsmServerPort = new HashMap<String, Integer>();
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void listenAndHandle() throws Exception
	{
		System.out.println("Nameserver waiting for requests...");

		while (true)
		{
			try (Socket socNameServiceUser = ssoNameServer.accept())
			{
				System.out.println("NameServer client connection from "
						+ socNameServiceUser.getRemoteSocketAddress().toString());
				ObjectInputStream inputStream = new ObjectInputStream(
						socNameServiceUser.getInputStream());
				ObjectOutputStream outputStream = new ObjectOutputStream(
						socNameServiceUser.getOutputStream());
				String command = (String) inputStream.readObject();
				String name = (String) inputStream.readObject();

				handleCommand(inputStream, outputStream, command, name);

				outputStream.close();
				inputStream.close();
				socNameServiceUser.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Handle the command given by the client.
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @param command
	 * @param name
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void handleCommand(ObjectInputStream inputStream, ObjectOutputStream outputStream,
			String command, String name) throws IOException, ClassNotFoundException
	{
		if (command.equals("PUT"))
		{
			@SuppressWarnings("rawtypes")
			Class c = (Class) inputStream.readObject();
			hsmServerClass.put(name, c);
			String serverIP = (String) inputStream.readObject();
			hsmServerIP.put(name, serverIP);
			int serverPort = inputStream.readInt();
			hsmServerPort.put(name, serverPort);
			outputStream.writeObject("OK");
		}
		else
		{
			@SuppressWarnings("rawtypes")
			Class c = hsmServerClass.get(name);
			outputStream.writeObject(c);
			String serverIP = hsmServerIP.get(name);
			outputStream.writeObject(serverIP);
			int serverPort = hsmServerPort.get(name);
			outputStream.writeInt(serverPort);
		}
	}

	public static void main(String[] args)
	{
		try
		{
			NameServer s = new NameServer(7090);
			s.listenAndHandle();
		}
		catch (BindException e)
		{
			System.out.println("Server already started");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
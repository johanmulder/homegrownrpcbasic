package nl.hanze.web.homegrownrpc.hello;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import nl.hanze.web.homegrownrpc.generic.Skel;

import org.apache.commons.codec.binary.Base64;

public class HelloSkel implements Skel<Hello>
{
	private int port;
	private Hello hello;
	private ServerSocket ssoHelloSkel;

	public void setPort(int port)
	{
		this.port = port;
	}

	public void setImplementation(Hello hello)
	{
		this.hello = hello;
	}

	public void listen() throws Exception
	{
		ssoHelloSkel = new ServerSocket(port);

		while (true)
		{
			// try-with-resource
			try (Socket socHelloSkel = ssoHelloSkel.accept())
			{
				OutputStreamWriter oswHelloSkel = new OutputStreamWriter(
						socHelloSkel.getOutputStream());
				BufferedReader bufHelloSkel = new BufferedReader(new InputStreamReader(
						socHelloSkel.getInputStream()));
				String strMethod = bufHelloSkel.readLine();

				// Determine what to do.
				if (strMethod.startsWith("sayHello#"))
					handleSayHello(oswHelloSkel, strMethod);
				else if (strMethod.startsWith("ageNextYear#1#int"))
					handleAgeNextYear(oswHelloSkel, strMethod);

				oswHelloSkel.flush();
			}
		}
	}

	/**
	 * Handle the ageNextYear method.
	 * 
	 * @param oswHelloSkel
	 * @param strMethod
	 * @throws Exception
	 * @throws IOException
	 */
	private void handleAgeNextYear(OutputStreamWriter oswHelloSkel, String strMethod)
			throws Exception, IOException
	{
		int param1 = Integer.parseInt(new String(Base64.decodeBase64(strMethod.substring(
				"ageNextYear#1#int".length() + 1).getBytes())));
		int value = hello.ageNextYear(param1);
		oswHelloSkel.write("int#" + Base64.encodeBase64String(Integer.toString(value).getBytes())
				+ "\n");
	}

	/**
	 * Handle the sayHello method.
	 * 
	 * @param oswHelloSkel
	 * @param strMethod
	 * @throws Exception
	 * @throws IOException
	 */
	private void handleSayHello(OutputStreamWriter oswHelloSkel, String strMethod)
			throws Exception, IOException
	{
		String[] methodInfo = strMethod.split("#");
		int numParams = Integer.parseInt(methodInfo[1]);

		// Yes, this should actually use reflection if we'd want it flexible,
		// but that goes beyond the scope of this assignment.
		if (numParams == 0)
		{
			String value = Base64.encodeBase64String(hello.sayHello().getBytes());
			oswHelloSkel.write("java.lang.String#" + value + "\n");
		}
		else if (numParams == 1)
			handleSayHelloWithOneParam(oswHelloSkel, strMethod);
		else if (numParams == 2)
		{
			if (strMethod.startsWith("sayHello#2#java.lang.String#int"))
			{
				String[] param = strMethod
						.substring("sayHello#2#java.lang.String#int".length() + 1).split("#");
				String param1 = new String(Base64.decodeBase64(param[0]));
				int param2 = Integer.valueOf(new String(Base64.decodeBase64(param[1])));
				String value = Base64.encodeBase64String(hello.sayHello(param1, param2).getBytes());
				oswHelloSkel.write("java.lang.String#" + value + "\n");
			}
		}
	}

	/**
	 * Handle the sayHello method with one parameter.
	 * 
	 * @param oswHelloSkel
	 * @param strMethod
	 * @throws Exception
	 * @throws IOException
	 */
	private void handleSayHelloWithOneParam(OutputStreamWriter oswHelloSkel, String strMethod)
			throws Exception, IOException
	{
		if (strMethod.startsWith("sayHello#1#java.lang.String"))
		{
			String param1 = strMethod.substring("sayHello#1#java.lang.String".length() + 1);
			param1 = new String(Base64.decodeBase64(param1));
			String value = Base64.encodeBase64String(hello.sayHello(param1).getBytes());
			oswHelloSkel.write("java.lang.String#" + value + "\n");

		}
		else if (strMethod.startsWith("sayHello#1#int"))
		{
			String[] param = strMethod.substring("sayHello#1#int".length() + 1).split("#");
			int param1 = Integer.valueOf(new String(Base64.decodeBase64(param[0])));
			String value = Base64.encodeBase64String(hello.sayHello(param1).getBytes());
			oswHelloSkel.write("java.lang.String#" + value + "\n");
		}
	}
}